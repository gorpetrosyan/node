
const argTypes = ['c', 'h', 't', 'l'];

const weatherApiUrl = 'https://api.openweathermap.org/data/2.5/weather'

const api_store_key_name = {
    token: 'token',
    city: 'city',
    lang: 'lang'
}

const units = {
    standard: 'standard',
    metric: 'metric',
    imperial: 'imperial'
}

const defaultLanguage = 'en';

const processArguments = process.argv

export { argTypes, weatherApiUrl, api_store_key_name, processArguments, defaultLanguage, units }
