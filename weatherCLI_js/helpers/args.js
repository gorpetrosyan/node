import { argTypes } from '../constants/index.js';

const getConsoleArgs = (globalArgs) => {
    const [execute, filerPath, ...args] = globalArgs;
    const response = {}
    args.forEach((item, index, array) => {
        if (item.charAt(0) === '-' && argTypes.includes(item.substring(1))) {
           if (index === array.length - 1) {
                response[item.substring(1)] = true;
            } else if (array[index + 1].charAt(0) !== '-') {
                response[item.substring(1)] = array[index + 1]
            } else if (argTypes.includes(item.substring(1))) {
                response[item.substring(1)] = true
            }
        }
    })
    return response;
}

export { getConsoleArgs }
