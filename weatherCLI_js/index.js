#!/usr/bin/env node
import {getConsoleArgs} from "./helpers/args.js";
import {printError, printSuccess, printHelper} from "./services/log.service.js";
import {saveValueByKey, getValueByKey} from './services/storage.service.js';
import {getWeatherForecastForCity} from "./services/weather-api.service.js";
import {api_store_key_name, processArguments, defaultLanguage} from './constants/index.js';

const initWeatherProgram = async () => {
    try {
        const args = getConsoleArgs(processArguments);
        if (Object.keys(args).length) {
            if (args.h) {
                printHelper();
            }
            if (args.t) {
                if (args.t.length) {
                    await saveValueByKey(api_store_key_name.token, args.t);
                    printSuccess('Token successfully stored;');
                }else{
                    printError('Token value is empty, use -h for help')
                }
            }
            if (args.l) {
                if (args.l.length) {
                    await saveValueByKey(api_store_key_name.lang, args.l);
                    printSuccess('Language successfully stored;');
                }else{
                    printError('Language value is empty, use -h for help')
                }
            }
            if (args.c) {
                const appId = await getValueByKey(api_store_key_name.token);
                if(appId && args.c.length){
                    const lang = await getValueByKey(api_store_key_name.lang) || defaultLanguage;
                    await saveValueByKey(api_store_key_name.city, args.c);
                    const { data } = await getWeatherForecastForCity(args.c, appId, lang);
                    console.log(data, 'result')
                }else{
                    printError('Please enter City name at first with -C [CITY]')
                }
            }
        } else {
            printError('missing prefixes, use -h for help')
        }
    } catch (e) {
        printError(e.response?.data?.message ?? e.message)
    }
}

await initWeatherProgram();
