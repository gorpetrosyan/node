import chalk from 'chalk';

/**
 * @param error
 */
const printError = (error) => {
    console.error(chalk.bgRed(chalk.underline("Error: ")) + error);
}

/**
 * @param message
 */
const printSuccess = (message) => {
    console.log(chalk.bgGreen('SUCCESS: ') + chalk.white(message));
}

/**
 */
const printHelper = ()=> {
    console.log(
        `${chalk.bgCyan('INFO')}
        -h [arg]: returns helper info.
        -c [CITY]: returns city's weather forecast.
        -t [API_KEY]: gets api key.
        -l [LANGUAGE]: Api language.
        `
    )
}

export { printSuccess, printError, printHelper }
