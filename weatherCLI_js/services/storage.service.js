import {join, resolve} from 'path';
import {promises} from 'fs';

const dataDir = join(resolve(''), '/database/weather-data.json');

/**
 * @param key
 * @param value
 * @returns {Promise<void>}
 */
const saveValueByKey = async (key, value) => {
    let data = {};
    if (await isExistFile(dataDir)) {
        const fileJsonData = await promises.readFile(dataDir);
        data = JSON.parse(fileJsonData.toString('utf-8'));
    }
    data[key] = value;
    await promises.writeFile(dataDir, JSON.stringify(data));
}

/**
 * @param key
 * @returns {Promise<{}|*>}
 */
const getValueByKey = async (key) => {
    if (await isExistFile(dataDir)) {
        const fileJsonData = await promises.readFile(dataDir);
        const data = JSON.parse(fileJsonData.toString('utf8'));
        return data[key];
    }
    return null;
}
/**
 * @param path
 * @returns {Promise<boolean>}
 */
const isExistFile = async (path) => {
    try {
        await promises.stat(path);
        return true;
    } catch (e) {
        return false;
    }
}


export {saveValueByKey, getValueByKey}
