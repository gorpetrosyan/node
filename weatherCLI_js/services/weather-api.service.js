import { weatherApiUrl, units } from '../constants/index.js';
import axios from "axios";

/**
 * @param axios
 * @param url
 * @returns {Promise<*>}
 */
const weatherForecastRequest = async (axios, url) => {
    return axios.get(url);
}

/**
 * @param city
 * @param appid
 * @param lang
 * @returns {Promise<*>}
 */
const getWeatherForecastForCity = async (city, appid, lang) => {
    const url = new URL(weatherApiUrl);
    url.searchParams.append('q', city);
    url.searchParams.append('appid', appid);
    url.searchParams.append('lang', lang);
    url.searchParams.append('units', units.metric);
    return await weatherForecastRequest(axios, url.href)
}

export { getWeatherForecastForCity }
